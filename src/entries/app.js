import 'normalize.css';

import '../components/common/common.scss';
import '../components/board/board.scss';
import '../components/tile/tile.scss';
import '../components/score/score.scss';
import '../components/finishScreen/finishScreen.scss';

import gameController from '../components/common/gameController';

document.addEventListener('DOMContentLoaded', () => {
    gameController();
});
