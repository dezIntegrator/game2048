let board;
let boardData;
let mouseStartPosX;
let mouseStartPosY;
let tilesMoved;
let scoreContainer;
let scoreValue;
let gameFinished;
let gameFinishedScreen;

// сгенерировать новую плитку
function generateTile() {
    const random = parseInt((Math.random() * 10), 10) + 1; // случайное число от 1 до 10
    const tileEl = document.createElement('div');
    let tileValue;

    if (random === 10) { // если 10 - плитка номинала 4. вероятность 1/10
        tileValue = 4;
    } else { // в остальных случаях - плитка номинала 2. вероятность 9/10
        tileValue = 2;
    }

    let ifTileAdded = false;

    while (!ifTileAdded) {
        const posTop = parseInt((Math.random() * 4), 10); // случайное число от 0 до 3
        const posLeft = parseInt((Math.random() * 4), 10);

        if (boardData[posTop][posLeft] === 0) { // если место свободно, добавляем плитку
            tileEl.className = `tile tile-${tileValue} tile-position-${posTop}-${posLeft}`;
            boardData[posTop][posLeft] = tileValue;
            ifTileAdded = true;
        }
    }

    board.appendChild(tileEl);
}

function removeTile(tileData) {
    scoreValue += tileData.value;
    scoreContainer.textContent = scoreValue;

    const tile = document.querySelector(`.tile.tile-${tileData.value / 2}.tile-position-${tileData.top}-${tileData.left}`);
    if (!tile) {
        return;
    }

    tile.parentNode.removeChild(tile);
}

// поиск места для плитки
function findPlace(tileTop, tileLeft, tileValue, moveParameters) {
    let newTile = { top: tileTop, left: tileLeft, value: tileValue };

    if (boardData[tileTop][tileLeft] === 0) {
        if (newTile.top + moveParameters.topOffset >= 0 &&
            newTile.top + moveParameters.topOffset <= 3) {
            newTile = findPlace(
                newTile.top + moveParameters.topOffset,
                newTile.left + moveParameters.leftOffset,
                tileValue,
                moveParameters,
            );
        }
    } else {
        if (boardData[tileTop][tileLeft] === tileValue) {
            setTimeout(removeTile, 100, newTile);
            newTile.value *= 2;
            if (newTile.value === 2048) {
                gameFinished = true;
            }
            return newTile;
        }
        newTile.top -= moveParameters.topOffset;
        newTile.left -= moveParameters.leftOffset;
        return newTile;
    }
    return newTile;
}

// установить положение плитки
function setTilePlace(top, left, moveParameters) {
    const tileValue = boardData[top][left];
    const newTile = findPlace(
        top + moveParameters.topOffset,
        left + moveParameters.leftOffset,
        tileValue,
        moveParameters,
    );
    let tile = document.querySelector(`.tile.tile-${tileValue}.tile-position-${top}-${left}`);

    if (!tile) {
        const tileEl = document.createElement('div');
        tileEl.className = `tile tile-${tileValue} tile-position-${top}-${left}`;
        board.appendChild(tileEl);
        tile = document.querySelector(`.tile.tile-${tileValue}.tile-position-${top}-${left}`);
    }

    if (top !== newTile.top || left !== newTile.left) {
        tile.className = `tile tile-${newTile.value} tile-position-${newTile.top}-${newTile.left}`;
        tilesMoved = true;
    }
    boardData[top][left] = 0;
    boardData[newTile.top][newTile.left] = newTile.value;
}

function checkGameFinish() {
    gameFinished = true;
    for (let left = 0; left < 4; left += 1) {
        for (let top = 0; top < 4; top += 1) {
            if (boardData[top][left] === 0) {
                gameFinished = false;
                return;
            }

            if (left !== 3) {
                if (boardData[top][left] === boardData[top][left + 1]) {
                    gameFinished = false;
                    return;
                }
            }

            if (top !== 3) {
                if (boardData[top][left] === boardData[top + 1][left]) {
                    gameFinished = false;
                    return;
                }
            }
        }
    }
}

// двигаем плитки
function move(moveParameters) {
    let top = moveParameters.startTop;
    let left = moveParameters.startLeft;
    let moved;

    while (!moved) {
        if (boardData[top][left] !== 0) {
            setTilePlace(
                top,
                left,
                moveParameters,
            );
        }

        top += moveParameters.topLoopDirection;

        if (top < 0 || top > 3) {
            top = moveParameters.startTop;
            left += moveParameters.leftLoopDirection;
            if (left < 0 || left > 3) {
                moved = true;
            }
        }
    }

    if (tilesMoved) {
        tilesMoved = false;
        generateTile();
        checkGameFinish();
        if (gameFinished) {
            gameFinishedScreen.className += ' finish-screen_displayed';
        }
    }
}

// Обработка свайпов мышкой
function bindSwipeEvents() {
    let ifMouseDown = false;

    board.addEventListener('mousedown', (e) => {
        if (gameFinished) {
            return;
        }

        ifMouseDown = true;
        mouseStartPosX = e.clientX;
        mouseStartPosY = e.clientY;
        e.preventDefault();
    });

    window.addEventListener('mouseup', (e) => {
        if (!ifMouseDown) {
            return;
        }
        ifMouseDown = false;

        const dx = e.clientX - mouseStartPosX;
        const absDx = Math.abs(dx);

        const dy = e.clientY - mouseStartPosY;
        const absDy = Math.abs(dy);

        if (Math.max(absDx, absDy) > 10) {
            if (absDx > absDy) {
                if (dx > 0) {
                    move({
                        topOffset: 0,
                        leftOffset: 1,
                        startTop: 0,
                        startLeft: 2,
                        topLoopDirection: 1,
                        leftLoopDirection: -1,
                    }); // right
                } else {
                    move({
                        topOffset: 0,
                        leftOffset: -1,
                        startTop: 0,
                        startLeft: 1,
                        topLoopDirection: 1,
                        leftLoopDirection: 1,
                    }); // left
                }
            }

            if (absDx < absDy) {
                if (dy > 0) {
                    move({
                        topOffset: 1,
                        leftOffset: 0,
                        startTop: 2,
                        startLeft: 0,
                        topLoopDirection: -1,
                        leftLoopDirection: 1,
                    }); // down
                } else {
                    move({
                        topOffset: -1,
                        leftOffset: 0,
                        startTop: 1,
                        startLeft: 0,
                        topLoopDirection: 1,
                        leftLoopDirection: 1,
                    }); // up
                }
            }
        }
    });
}

// инициализация игрового поля
function initializeGame() {
    board = document.querySelector('.board');
    boardData = [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ];
    scoreContainer = document.querySelector('.score__value');
    scoreValue = 0;
    gameFinishedScreen = document.querySelector('.finish-screen');

    bindSwipeEvents();
    generateTile();
}

export default initializeGame;
