const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HandlebarsPlugin = require('handlebars-webpack-plugin');
const path = require('path');

module.exports = {
    entry: './src/entries/app.js',
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, 'build'),
    },
    devServer: {
        open: true,
        overlay: true,
        watchContentBase: true,
        watchOptions: {
            poll: 1000,
        },
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'eslint-loader',
                options: {
                    failOnError: true,
                },
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env'],
                    },
                },
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    publicPath: '../',
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader'],
                }),
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader',
                }),
            },
        ],
    },
    plugins: [
        new ExtractTextPlugin('./css/[name].css'),
        new HandlebarsPlugin({
            entry: path.join(process.cwd(), 'src/pages', '*.hbs'),
            output: path.join(process.cwd(), 'build', '[name].html'),
            partials: [
                path.join(process.cwd(), 'src/components', '*', '*.hbs'),
            ],
        }),
        new webpack.HotModuleReplacementPlugin({}),
    ],
};
